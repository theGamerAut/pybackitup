from unittest import TestCase
from classes.backup_config import BackupConfig
import configparser
import os


class TestBackupConfig(TestCase):

    def test_read_config(self):
        # write demo file
        config = configparser.ConfigParser()
        config['DEFAULT'] = {'ServerAliveInterval': '45',
                             'Compression': 'yes',
                             'CompressionLevel': '9'}
        config['bitbucket.org'] = {}
        config['bitbucket.org']['User'] = 'hg'
        config['topsecret.server.com'] = {}
        topsecret = config['topsecret.server.com']
        topsecret['Port'] = '50022'  # mutates the parser
        topsecret['ForwardX11'] = 'no'  # same here
        config['DEFAULT']['ForwardX11'] = 'yes'
        with open('example.ini', 'w') as configfile:
            config.write(configfile)

        # read demo file with to test methode
        config_read = BackupConfig('example.ini')

        self.assertEqual(config_read.parameters, config._sections)

        os.remove('example.ini')

    def test_write_config(self):
        # write demo file
        config = configparser.ConfigParser()
        config['DEFAULT'] = {'ServerAliveInterval': '45',
                             'Compression': 'yes',
                             'CompressionLevel': '9'}
        config['bitbucket.org'] = {}
        config['bitbucket.org']['User'] = 'hg'
        config['topsecret.server.com'] = {}
        topsecret = config['topsecret.server.com']
        topsecret['Port'] = '50022'  # mutates the parser
        topsecret['ForwardX11'] = 'no'  # same here
        config['DEFAULT']['ForwardX11'] = 'yes'
        with open('example.ini', 'w') as configfile:
            config.write(configfile)

        # read demo file with to test methode
        config_read = BackupConfig('example.ini')

        config_read.file = 'example2.ini'
        config_read.write_config()

        config_read_2 = BackupConfig('example2.ini')
        self.assertEqual(config_read_2.parameters, config_read.parameters)

        os.remove('example.ini')
        os.remove('example2.ini')


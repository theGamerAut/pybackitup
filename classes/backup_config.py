import configparser


class BackupConfig(object):

    def __init__(self, file):
        self.file = file
        self.config = self.read_config()
        self.l_sections = self.config.sections()
        self.parameters = {}
        for x in self.config.sections():
            self.parameters[x] = self.config._sections[x]

    def read_config(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read(self.file)
        print(config)
        return config

    def write_config(self):
        config = self.config
        with open(self.file, 'w') as configfile:
            config.write(configfile)

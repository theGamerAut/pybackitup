import argparse
import logging
import os
import tarfile
import datetime
import time

from classes.backup_config import BackupConfig

"""
### Arguments
"""


parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true')


"""
### Configuration
"""


if parser.parse_args().debug:
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

"""
### functions
"""


def create_backup(path_in, path_out, compress=True):
    now = datetime.datetime.now()
    path_out = path_out + \
               "\\" + \
               now.strftime("%Y%m%d-%H%M%S") + \
               '_' + \
               os.path.basename(path_in)

    logging.debug('New path: ' + path_out)
    logging.info('Start with backup: ' + os.path.basename(path_in))
    if compress:
        tar = tarfile.open(path_out + '.tar.gz', "w:gz")
        tar.add(path_in)
        tar.close()
    else:
        tar = tarfile.open(path_out + '.tar', "w")
        tar.add(path_in)
        tar.close()
    logging.info('Finished with backup: ' + os.path.basename(path_in))


"""
### Main program
"""


if __name__ == "__main__":
    config = BackupConfig('pybackitup.ini')

    while True:
        for backup in config.l_sections:
            if backup != 'default':
                logging.debug('Found backup')
                create_backup(config.config.get(backup, 'src'),
                              config.config.get(backup, 'dst'),
                              config.config.getboolean(backup, 'compress'))
        time.sleep(int(config.config.get('default', 'interval')) * 60)
